// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Voting-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-voting-integration>.
//
// Bot-Voting-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Voting-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Voting-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandBuilder } from "@discordjs/builders";
import { BotLogger, BotModule, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CacheType, CommandInteractionOption, CommandInteractionOptionResolver } from "discord.js";
import { inject, injectable } from "inversify";
import { schedule, ScheduledTask } from "node-cron";
import { Mutex } from "async-mutex";
import { DASServerCommand } from "./commands/deutscheArkServerCommand";
import { VoteCommand } from "./commands/voteCommand";
import { DeutscheArkServerIdProviderIfc } from "./interfaces/deutscheArkServerIdProviderIfc";
import { TYPES_ARKVOTINGINTEGRATION } from "./TYPES";
import { ArkVotingIntegrationConfigDatabaseProvider } from "./database/arkVotingIntegrationConfigDatabaseProvider";
import { DeutscheArkServerDatabaseProvider } from "./database/deutscheArkServerDatabaseProvider";
import { VotingHistoryService } from "./services/votingHistory.service";
import { DeutscheArkServerVotingService } from "./services/deutscheArkserverVoting.service";
import { ArkVotingIntegrationConfig } from "./models/arkVotingIntegrationConfig";
import { ArkServersNetVotingService } from "./services/arkServersNetVoting.service";
import { ArkServersNetCommand } from "./commands/arkServersNetCommand";
import { ArkServersNetServerDatabaseProvider } from "./database/arkServersNetServerDatabaseProvider";

@injectable()
export class ArkVotingIntegrationBotModule extends BotModule {
    constructor(
        @inject(TYPES_ARKVOTINGINTEGRATION.ArkVotingConfigDatabaseProvider) private _arkVotingIntegrationConfigProvider: ArkVotingIntegrationConfigDatabaseProvider,
        @inject(TYPES_ARKVOTINGINTEGRATION.DeutscheArkServerDatabaseProvider) private _deutscheArkServerProvider: DeutscheArkServerDatabaseProvider,
        @inject(TYPES_ARKVOTINGINTEGRATION.ArkServersNetServerDatabaseProvider) private _arkServersNetServerDatabaseProvider: ArkServersNetServerDatabaseProvider,
        @inject(TYPES_ARKVOTINGINTEGRATION.DeutscheArkServerIdProvider) private _deutscheArkServerIdProvider: DeutscheArkServerIdProviderIfc,
        @inject(TYPES_ARKVOTINGINTEGRATION.VotingHistoryService) private _votingHistoryService: VotingHistoryService,
        @inject(TYPES_ARKVOTINGINTEGRATION.DeutscheArkServerVotingService) private _deutscheArkServerVotingService: DeutscheArkServerVotingService,
        @inject(TYPES_ARKVOTINGINTEGRATION.ArkServersNetVotingService) private _arkServersNetVotingService: ArkServersNetVotingService,
        @inject(TYPES_BOTRUNNER.BotLogger) _botLogger: BotLogger
        ) {
        super('arkvoting', 'Integration of multiple Ark Voting websites which credits to TCsAR API.', _botLogger);
    }
    private _voteCheckTask: ScheduledTask | undefined = undefined;
    private _updateMutex = new Mutex();
    
    async initializeCore(): Promise<void> {
        this.addSubCommand(new DASServerCommand(this._deutscheArkServerProvider))
        this.addSubCommand(new ArkServersNetCommand(this._arkServersNetServerDatabaseProvider))
        this.addSubCommand(new VoteCommand(this._votingHistoryService, this._deutscheArkServerIdProvider))
    }
    
    async start(): Promise<void> {
        this.logger.info('ArkVotingBotModule: Start');

        if (this._voteCheckTask == undefined) {
            this.logger.info('Scheduling Vote Check Task.');
            this._voteCheckTask = schedule('0 * * * *', () => {
                this.runVotingCheck().then()
            });
            this._voteCheckTask.start();
        }
    }
    
    async stop(): Promise<void> {
        this.logger.info('ArkVotingBotModule: Stop');
        if (this._voteCheckTask != undefined) {
            this._voteCheckTask.stop();
            this._voteCheckTask = undefined;
        }
    }
    
    async buildConfigCommand(configCommandBuilder: SlashCommandBuilder): Promise<void> {
        configCommandBuilder.addSubcommand(scmd =>
            scmd.setName(this.name)
            .setDescription('Module specific configuration')

            .addStringOption(opt =>
                opt.setName('coinspervote')
                .setDescription('How many coins should be credited per vote?')
                .setRequired(true)
            )
            .addBooleanOption(opt =>
                opt.setName('votelogpublic')
                .setDescription('Do you want to publically show the vote logging?')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('votelogcategory')
                .setDescription('What is the category name for vote logging? Recommended: STATUS')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('votelogchannel')
                .setDescription('What is the channel name for vote logging? Recommended: voting')
                .setRequired(true)
            )
        );
    }

    async handleConfigCommand(interaction: BaseCommandInteraction<CacheType>): Promise<void> {
        if (interaction.guild == null) return;
        
        const opts = <CommandInteractionOptionResolver>interaction.options;
        const subCommand = <CommandInteractionOption[]>opts.data;
        const options = <CommandInteractionOption[]>subCommand[0].options;
        
        const vals = new ArkVotingIntegrationConfig();
        vals.guildId = interaction.guild.id;
        
        for(const o of options) {
            if (o.name === 'coinspervote') vals.coinsPerVote = <number>o.value;
            if (o.name === 'votelogpublic') vals.logPublic = <boolean>o.value;
            if (o.name === 'votelogcategory') vals.logCategory = <string>o.value;
            if (o.name === 'votelogchannel') vals.logChannel = <string>o.value;

            this.logger.debug(`Configured option: ${o.name}. Value: ${o.value}`);
        }

        await this._arkVotingIntegrationConfigProvider.store(vals, true);
     
        // restart
        await this.stop();
        await this.start();

        await interaction.reply(`Configuration for ${this.name} saved!`);
    }

    private async runVotingCheck(): Promise<void> {
        this.logger.info('Running Vote Update.');
        const release = await this._updateMutex.acquire();
        try {
            await this._deutscheArkServerVotingService.runVotingUpdate(this.client);
            await this._arkServersNetVotingService.runVotingUpdate(this.client);
        } finally {
            release();
            this.logger.info('Finished Vote Update.');
        }
    }
}