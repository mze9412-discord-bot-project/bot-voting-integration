// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Voting-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-voting-integration>.
//
// Bot-Voting-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Voting-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Voting-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { VotingHistoryService } from "../services/votingHistory.service";
import { DeutscheArkServerIdProviderIfc } from "../interfaces/deutscheArkServerIdProviderIfc";

export class VoteCommand extends BotCommand {
    constructor(private _votingHistoryService: VotingHistoryService, private _idProvider: DeutscheArkServerIdProviderIfc) {
        super('history', 'Inspect existing votes and vote statistics.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('statistics')
                .setDescription('Print statistics for your voting history.')
                .addStringOption(o =>
                    o.setName('month')
                        .setDescription('For which month do you want to see the voting statistics?')
                        .addChoices(
                            { name: '01', value: "01"},
                            { name: '02', value: "02"},
                            { name: '03', value: "03"},
                            { name: '04', value: "04"},
                            { name: '05', value: "05"},
                            { name: '06', value: "06"},
                            { name: '07', value: "07"},
                            { name: '08', value: "08"},
                            { name: '09', value: "09"},
                            { name: '10', value: "10"},
                            { name: '11', value: "11"},
                            { name: '12', value: "12"}
                        )
                        .setRequired(true)
                )
                .addStringOption(o =>
                    o.setName('year')
                        .setDescription('For which year do you want to see the voting statistics?')
                        .addChoices(
                            { name: '2020', value: "2020"},
                            { name: '2021', value: "2021"},
                            { name: '2022', value: "2022"},
                            { name: '2023', value: "2023"}
                        )
                        .setRequired(true)
                )
            );
            
        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('player')
                .setDescription('Shows the voting statistics for one of your players.')
                .addStringOption(o =>
                    o.setName('steamid')
                        .setDescription('What is the player\'s steam id?')
                        .setRequired(true)
                )
            );

        return groupCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'statistics') {
            await this.handleStatistics(interaction);   
        } else if (subCommand.name === 'player') {
            await this.handlePlayer(interaction);
        }
    }

    private async handlePlayer(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get steamid
        let steamid = '';
        const guildId = interaction.guild.id;
        for (const o of parameters) {
            if (o.name === 'steamid') steamid = <string>o.value;
        }

        // get history for steam id
        const history = await this._votingHistoryService.getVotingHistory(guildId, steamid);

        // print to buffer
        const buffer: string[] = [];
        buffer.push(`**Voting history for *${history.steamId}***`);
        buffer.push(`#Votes: ${history.votes.length}`);

        // count votes per month
        const statistics = new Map<string, number>();
        for (const vote of history.votes) {
            const date = new Date(vote.date);
            const key = `${date.getFullYear()}/${date.getMonth()+1}`;
            
            // add entry if not yet present
            if (!statistics.has(key))
            {
                statistics.set(key, 0);
            }

            // increase vote counter
            const count = <number>statistics.get(key) + 1;
            statistics.set(key, count);
        }

        // add vote statistics
        for (const key of statistics.keys()) {
            const entry = statistics.get(key);
            buffer.push(`- ${key}: ${entry}`)
        }

        // post to discord
        const split = Util.splitMessage(buffer.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleStatistics(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        // get month and year
        let month = '';
        let year = '';
        const guildId = interaction.guild.id;
        for (const o of parameters) {
            if (o.name === 'month') month = <string>o.value;
            if (o.name === 'year') year = <string>o.value;
        }

        if (month === '' || year === '') {
            await interaction.reply(`Sorry, these are not valid values for month or year.`);
        } else {
            await interaction.reply({ content: `Fetching data. Please stand by ...`, ephemeral: true });

            const votes = await this._votingHistoryService.getVotingHistoryForMonth(guildId, month, year);
            if (votes.length > 0) {
                const buffer: string[] = [];

                // count all votes
                let totalCount = 0;
                for (const vote of votes) {
                    totalCount = totalCount + vote.votes.length;
                }

                // sort
                const sortedByVotes = votes.sort((a, b) => {
                    if(a.votes.length > b.votes.length) {
                        return -1
                    } else if(a.votes.length < b.votes.length) {
                        return 1;
                    }
                    return 0;
                });

                // add header
                buffer.push(`**Votes for *${year}/${month}*: ${totalCount}**`);

                // add sorted list to buffer
                let i = 1;
                for (const vote of sortedByVotes) {
                    // get voting name for steamid
                    const name = await this._idProvider.getUsernameForSteamId(guildId, vote.steamId);

                    // add formatted
                    buffer.push(`*${i}) ${name} (${vote.steamId})*: ${vote.votes.length}`);

                    // increase counter
                    i++;
                }
                
                // post to discord
                const split = Util.splitMessage(buffer.join('\n'));
                for (const part of split) {
                    await interaction.followUp(part);
                }
            } else {
                await  interaction.followUp(`No votes found for ${year}/${month}.`);
            }
        }
    }
}