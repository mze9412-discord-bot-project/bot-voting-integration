// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Voting-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-voting-integration>.
//
// Bot-Voting-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Voting-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Voting-Integration. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { DeutscheArkServerDatabaseProvider } from "../database/deutscheArkServerDatabaseProvider";
import { DeutscheArkServer } from "../models/deutscheArkServer";

export class DASServerCommand extends BotCommand {
    constructor(private _serverDbProvider: DeutscheArkServerDatabaseProvider) {
        super('das', 'Manage your list of registered deutsche ark servers.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('add')
                .setDescription('Add a new server to your registered DAS servers.')
                .addStringOption(o =>
                    o.setName('serverid')
                        .setDescription('ID of your server at Deutsche Arkserver.')
                        .setRequired(true)
                )
                .addStringOption(o =>
                    o.setName('apikey')
                        .setDescription('API key of your server.')
                        .setRequired(true)
                )
            );
            
        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('delete')
                .setDescription('Delete one of your registered Ark servers.')
                .addStringOption(o =>
                    o.setName('name')
                        .setDescription('Name of your server.')
                        .setRequired(true)
                )
            );
        
        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('list')
                .setDescription('List your registered Ark servers.')
            );

        return groupCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'list') {
            await this.handleServerList(interaction);   
        } else if (subCommand.name === 'add') {
            await this.handleServerAdd(interaction);
        } else if (subCommand.name === 'delete') {
            await this.handleServerDelete(interaction);
        }
    }

    private async handleServerList(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        
        const servers = await this._serverDbProvider.getAll(interaction.guild.id);

        if(servers.length === 0) {
            await interaction.reply('There are no servers. Sorry! Maybe you want to add some with the *add* command?');
            return;
        }
        
        const serverEntries: string[] = [];
        serverEntries.push(`There are ${servers.length} servers. Here's a list for you:`);
        for(let i = 0; i < servers.length; i++) {
            const server = servers[i];
            serverEntries.push(`${(i+1)}) ${server.serverId} / ${server.serverAPIKey}.`);
        }
        
        const split = Util.splitMessage(serverEntries.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleServerAdd(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        const server = new DeutscheArkServer();
        server.guildId = interaction.guild.id;
        for (const o of parameters) {
            if (o.name === 'serverid') server.serverId = <string>o.value;
            if (o.name === 'apikey') server.serverAPIKey = <string>o.value;
        }
        
        const success = await this._serverDbProvider.store(server, false);
        if (success) {
            await interaction.reply(`The server with the ID *${server.serverId}* was successfully added.`);
        } else {
            await interaction.reply(`There is already a server with the ID *${server.serverId}*.`);
        }
    }

    private async handleServerDelete(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;
        
        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        const id = <string>parameters[0].value;
        
        // get server from DB
        const server = await this._serverDbProvider.get(interaction.guild.id, id);
        if (server == null) {
            await interaction.reply(`There is no server with the ID *${id}*.`);
            return;
        }

        // delete server
        const success = await this._serverDbProvider.delete(server) === 1;
        if (success) {
            await interaction.reply(`The server *${id}* was successfully deleted.`);
        } else {
            await interaction.reply(`Failed to deleted server *${id}*.`);
        }
    }
}