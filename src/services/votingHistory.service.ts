import { BotLogger, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { inject, injectable } from "inversify";
import { Logger } from "tslog";
import { ArkServerVotingHistoryDatabaseProvider } from "../database/arkServerVotingHistoryDatabaseProvider";
import { ArkServerVote } from "../models/arkServerVote";
import { ArkServersVotingHistory } from "../models/arkServerVotingHistory";
import { TYPES_ARKVOTINGINTEGRATION } from "../TYPES";

@injectable()
export class VotingHistoryService {
    private logger!: Logger;

    constructor(
        @inject(TYPES_ARKVOTINGINTEGRATION.ArkVotingHistoryDatabaseProvider) private _arkVotingHistoryDatabaseProvider: ArkServerVotingHistoryDatabaseProvider,
        @inject(TYPES_BOTRUNNER.BotLogger) _botLogger: BotLogger
    ) {
        this.logger = _botLogger.getChildLogger('VotingHistoryService');
    }

    async getVotingHistory(guildId: string, steamId: string): Promise<ArkServersVotingHistory> {
        let history = await this._arkVotingHistoryDatabaseProvider.get(guildId, steamId);

        if (history == null) {
            history = new ArkServersVotingHistory();
            history.guildId = guildId;
            history.steamId = steamId;
        }

        return history;
    }

    async getVotingHistoryForMonth(guildId: string, month: string, year: string): Promise<ArkServersVotingHistory[]>
    {
        const numMonth = Number.parseInt(month) - 1; // getMonth in date counts from 0-11, not 1-12
        const numYear = Number.parseInt(year);

        const histories = await this._arkVotingHistoryDatabaseProvider.getAll(guildId);


        // create copies which only contain the relevant vote entries
        const filtered: ArkServersVotingHistory[] = [];
        for (const history of histories) {
            const filter = new ArkServersVotingHistory();
            filter.guildId = history.guildId;
            filter.steamId = history.steamId;

            // check votes
            this.logger.debug(`Voting: History for ${history.steamId}. Count: ${history.votes.length}`);
            for (const vote of history.votes) {
                const date = new Date(vote.date);
                this.logger.debug(`${date.getMonth()} === ${numMonth} && ${date.getFullYear()} === ${numYear} => ${date.getMonth() === numMonth && date.getFullYear() === numYear}`)
                if (date.getMonth() === numMonth && date.getFullYear() === numYear) {
                    filter.votes.push(vote);
                }
            }

            // add only if there are votes
            if (filter.votes.length > 0) filtered.push(filter);
        }

        return filtered;
    }

    async addVote(guildId: string, steamId: string, vote: ArkServerVote): Promise<boolean> {
        const history = await this.getVotingHistory(guildId, steamId);
        if (await this.hasVote(history, vote)) {
            return false;
        }

        history.votes.push(vote);
        return await this._arkVotingHistoryDatabaseProvider.store(history, true);
    }

    async hasVote(history: ArkServersVotingHistory, vote: ArkServerVote): Promise<boolean> {
        for (const v of history.votes) {
            if (v.serverId === vote.serverId && v.date === vote.date) return true;
        }

        return false;
    }
}