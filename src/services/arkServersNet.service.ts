import { inject, injectable } from "inversify";
import { ArkServersNetServerDatabaseProvider } from "../database/arkServersNetServerDatabaseProvider";
import { ArkServersNetServer } from "../models/arkServersNetServer";
import { TYPES_ARKVOTINGINTEGRATION } from "../TYPES";

@injectable()
export class ArkServersNetService {
    constructor(
        @inject(TYPES_ARKVOTINGINTEGRATION.ArkServersNetServerDatabaseProvider) private _arkServerDatabaseProvider: ArkServersNetServerDatabaseProvider
    ) { }

    async addServer(server: ArkServersNetServer): Promise<boolean> {
        return await this._arkServerDatabaseProvider.store(server, false);
    }

    async deleteServer(guildId: string, serverId: string): Promise<boolean> {
        const server = new ArkServersNetServer();
        server.guildId = guildId;
        server.serverId = serverId;
        return await this._arkServerDatabaseProvider.delete(server) === 1;
    }

    async getAll(guildId: string): Promise<ArkServersNetServer[]> {
        return await this._arkServerDatabaseProvider.getAll(guildId);
    }
}