// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Voting-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-voting-integration>.
//
// Bot-Voting-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Voting-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Voting-Integration. If not, see <http://www.gnu.org/licenses/>.

import { inject, injectable } from "inversify";
import { DeutscheArkServerDatabaseProvider } from "../database/deutscheArkServerDatabaseProvider";
import { DeutscheArkServer } from "../models/deutscheArkServer";
import { TYPES_ARKVOTINGINTEGRATION } from "../TYPES";

@injectable()
export class DeutscheArkServerService {
    constructor(
        @inject(TYPES_ARKVOTINGINTEGRATION.DeutscheArkServerDatabaseProvider) private _arkServerDatabaseProvider: DeutscheArkServerDatabaseProvider
    ) { }

    async addServer(server: DeutscheArkServer): Promise<boolean> {
        return await this._arkServerDatabaseProvider.store(server, false);
    }

    async deleteServer(guildId: string, serverId: string): Promise<boolean> {
        const server = new DeutscheArkServer();
        server.guildId = guildId;
        server.serverId = serverId;
        return await this._arkServerDatabaseProvider.delete(server) === 1;
    }

    async getAll(guildId: string): Promise<DeutscheArkServer[]> {
        return await this._arkServerDatabaseProvider.getAll(guildId);
    }
}