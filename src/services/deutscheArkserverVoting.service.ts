// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Voting-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-voting-integration>.
//
// Bot-Voting-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Voting-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Voting-Integration. If not, see <http://www.gnu.org/licenses/>.

import axios from 'axios';
import { inject, injectable } from "inversify";
import { DeutscheArkServerService } from "./deutscheArkServer.service";
import { VotingHistoryService } from "./votingHistory.service";
import { Client } from "discord.js";
import { TYPES_BOTRUNNER, DiscordChannelService, DiscordMessageService, BotLogger } from "@mze9412-discord-bot-project/bot-runner"
import { TCsARIntegrationPlayerService, TYPES_TCSARINTEGRATION } from "@mze9412-discord-bot-project/bot-tcsar-integration"
import { DeutscheArkServerIdProviderIfc } from '../interfaces/deutscheArkServerIdProviderIfc';
import { ArkVotingIntegrationConfigDatabaseProvider } from '../database/arkVotingIntegrationConfigDatabaseProvider';
import { TYPES_ARKVOTINGINTEGRATION } from '../TYPES';
import { DeutscheArkServer } from '../models/deutscheArkServer';
import { ArkServerVote } from '../models/arkServerVote';
import { Logger } from 'tslog';

@injectable()
export class DeutscheArkServerVotingService {
    private logger!: Logger;

    constructor(
        @inject(TYPES_ARKVOTINGINTEGRATION.ArkVotingConfigDatabaseProvider) private _arkVotingConfigProvider: ArkVotingIntegrationConfigDatabaseProvider,
        @inject(TYPES_ARKVOTINGINTEGRATION.DeutscheArkServerService) private _deutscheArkServerService: DeutscheArkServerService,
        @inject(TYPES_ARKVOTINGINTEGRATION.VotingHistoryService) private _votingHistoryService: VotingHistoryService,
        @inject(TYPES_ARKVOTINGINTEGRATION.DeutscheArkServerIdProvider) private _idProvider: DeutscheArkServerIdProviderIfc,
        @inject(TYPES_TCSARINTEGRATION.TCsARIntegrationPlayerService) private _tcsarPlayerService: TCsARIntegrationPlayerService,
        @inject(TYPES_BOTRUNNER.DiscordChannelService) private _discordChannelService: DiscordChannelService,
        @inject(TYPES_BOTRUNNER.DiscordMessageService) private _discordMessageService: DiscordMessageService,
        @inject(TYPES_BOTRUNNER.BotLogger) _botLogger: BotLogger
    ) {
        this.logger = _botLogger.getChildLogger('VotingHistoryService');
    }

    public async runVotingUpdate(client: Client): Promise<void> {
        for(const guild of client.guilds.cache.values()) {
            await this.runVotingUpdateForGuild(guild.id);
        }
    }

    private async runVotingUpdateForGuild(guildId: string) {
        // abort if voting is not active
        this.logger.info(`Voting: Running for guild ${guildId}.`);

        // buffer for result message
        const buffer: string[] = [];

        // get servers for guild
        const servers = await this._deutscheArkServerService.getAll(guildId);
        
        // get config
        const config = await this._arkVotingConfigProvider.get(guildId);
        if (config == null) return;

        // run for server        
        for (const server of servers) {
            const serverBuffer = await this.runVotingUpdateForServer(server, config.coinsPerVote);
            buffer.push(serverBuffer.join('\n'));
            this.logger.info('Voting: Appended serverBuffer to buffer!');
        }

        // send message to channel, create category and channel if necessary
        const msg = buffer.join('\n');
        this.logger.debug(`Voting:\n########## MSG START ##########\n${msg}\n########## MSG END ##########`);
        if (msg.length > 0) {
            this.logger.debug('Voting: Message is long enough. Trying to get category and channel.')
            let category = await this._discordChannelService.getCategoryByName(guildId, config.logCategory);
            if (category == undefined) {
                this.logger.debug("Voting: Creating category ...");
                category = await this._discordChannelService.createCategory(guildId, config.logCategory);
            }

            let channel = await this._discordChannelService.getTextChannelByName(guildId, config.logChannel);
            if (channel == undefined) {
                this.logger.debug("Voting: Creating channel ...");
                channel = await this._discordChannelService.createTextChannel(guildId, config.logChannel, category);
                if (!config.logPublic) await channel.permissionOverwrites.edit(channel.guild.roles.everyone, { VIEW_CHANNEL: false });
            }

            this.logger.debug(`Voting: Channel: ${channel} MsgLength: ${msg.length}`);
            this.logger.debug(`Voting: MessageService: ${this._discordMessageService}`);
            await this._discordMessageService.sendTextMessage(channel, msg);
        }
    }

    private async runVotingUpdateForServer(server: DeutscheArkServer, coinsPerVote: number): Promise<string[]> {
        const buffer: string[] = [];

        if (server.serverId === '' || server.serverAPIKey === '') {
            return buffer;
        }
        
        this.logger.info(`Voting: Running for ${server.serverId}.`);
        
        // get voting data for server
        const url = `https://deutsche-arkserver.de/api/votes/${server.serverId}/${server.serverAPIKey}`;
        const result = await axios.get(url);

        // check result
        let jsonData = undefined;
        if (result.status === 200) {
            jsonData = result.data;
        } else {
            buffer.push(`Voting: Failed to fetch voting data for *${server.serverId}*.`);
            return buffer;
        }

        // iterate over results
        for (const entry of jsonData) {
            const userOrig = (<string>entry.username);
            if (userOrig == null || userOrig == undefined) {
                continue;
            }

            const username = userOrig.toLowerCase();
            const dateString = <string>entry.votedatum;
            const dateParts = dateString.split('-');
            const date = new Date(Number(dateParts[0]), Number(dateParts[1])-1, Number(dateParts[2]));

            // get steamId from provider
            const steamId = await this._idProvider.getSteamIdForUsername(server.guildId, username);

            if (steamId.length > 0) {
                this.logger.debug(`Voting: SteamId: ${steamId} for ${username}`);
                
                // create a vote
                const vote = new ArkServerVote();
                vote.serverId = server.serverId;
                vote.date = date.getTime();

                // try to add the vote
                const success = await this._votingHistoryService.addVote(server.guildId, steamId, vote);

                // if it was successful, credit coins, if not, vote was already added before
                if(success) {
                    const result = await this._tcsarPlayerService.updateBalance(server.guildId, steamId, coinsPerVote);
                    if(result == undefined) {
                        this.logger.warn(`Voting: Failed to credit ${coinsPerVote} ARc for ${steamId}`);
                        buffer.push(`DAS: ${username} (${steamId}) voted for ${server.serverId}. Failed to update balance! You have to manually credit ${coinsPerVote} ARc!`);
                    } else {
                        this.logger.debug(`Voting: Credited ${coinsPerVote} ARc for ${steamId}`);
                        buffer.push(`DAS: ${username} (${steamId}) voted for ${server.serverId}. New balance: ${result} ARc.`);
                    }
                } else {
                    this.logger.debug('Voting: Vote already counted.');
                }
            } else {
                this.logger.debug(`Voting: No SteamId found for username ${userOrig}`);
                buffer.push(`DAS: No SteamId found for username ${userOrig}.`);
            }
        }
        return buffer;
    }
}