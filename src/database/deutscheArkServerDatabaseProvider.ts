// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Voting-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-voting-integration>.
//
// Bot-Voting-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Voting-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Voting-Integration. If not, see <http://www.gnu.org/licenses/>.

import { DatabaseProviderBase } from "@mze9412-discord-bot-project/bot-database-interface";
import { BotLogger, ConfigService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { inject, injectable } from "inversify";
import { DeutscheArkServer } from "../models/deutscheArkServer";

@injectable()
export class DeutscheArkServerDatabaseProvider extends DatabaseProviderBase<DeutscheArkServer> {
    constructor(
        @inject(TYPES_BOTRUNNER.ConfigService) configService: ConfigService,
        @inject(TYPES_BOTRUNNER.BotLogger) botLogger: BotLogger
    ) {
        super(configService, 'DeutscheArkServer', 'DeutscheArkServer', botLogger);
    }

    protected async storeCore(data: DeutscheArkServer, overwriteIfExists: boolean): Promise<boolean> {
        const exists = await this.exists(data);

        if (exists && overwriteIfExists) {
            await this.deleteCore(data);
        } else if (exists) {
            return false;
        }

        const client = await this.connect();
        const collection = await this.openCollection(client);

        const insertResult = await collection.insertOne(data);
        
        await client.close();        
        return insertResult.acknowledged;
    }

    protected async existsCore(data: DeutscheArkServer): Promise<boolean> {
        return await this.get(data.guildId, data.serverId) != undefined;        
    }

    public async get(guildId: string, serverId: string): Promise<DeutscheArkServer | null> {
        const release = await this.mutex.acquire();
        try {
            const client = await this.connect();
            const collection = await this.openCollection(client);

            const result = await collection.findOne({ guildId: guildId, serverId: serverId });
            
            await client.close();
            return result;
        } finally {
            release();
        }
    }

    public async getAll(guildId: string): Promise<DeutscheArkServer[]> {
        const release = await this.mutex.acquire();
        try {
            const client = await this.connect();
            const collection = await this.openCollection(client);

            const result = await collection.find({guildId: guildId}).toArray();
            
            await client.close();
            return result;
        } finally {
            release();
        }
    }
    
    protected async deleteCore(data: DeutscheArkServer): Promise<number> {
        const client = await this.connect();
        const collection = await this.openCollection(client);

        const deleteResult = await collection.deleteMany({guildId: data.guildId, serverId: data.serverId});

        await client.close();
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }

    protected async deleteAllCore(guildId: string): Promise<number> {
        const client = await this.connect();
        const collection = await this.openCollection(client);

        const deleteResult = await collection.deleteMany({guildId: guildId});

        await client.close();
        return deleteResult.deletedCount == undefined ? 0 : <number>deleteResult.deletedCount;
    }
    
    protected async createIndexCore(): Promise<void> {
        const client = await this.connect();
        const collection = await this.openCollection(client);

        await collection.createIndex({ guildId: 1, serverId: 1 });
        await client.close();
    }
}