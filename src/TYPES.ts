// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Voting-Integration <https://gitlab.com/mze9412-discord-bot-project/bot-voting-integration>.
//
// Bot-Voting-Integration is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Voting-Integration is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Voting-Integration. If not, see <http://www.gnu.org/licenses/>.

const TYPES_ARKVOTINGINTEGRATION = {
    ArkVotingConfigDatabaseProvider: Symbol.for("ArkVotingConfigDatabaseProvider"),
    ArkVotingHistoryDatabaseProvider: Symbol.for("ArkVotingHistoryDatabaseProvider"),
    DeutscheArkServerDatabaseProvider: Symbol.for("DeutscheArkServerDatabaseProvider"),
    ArkServersNetServerDatabaseProvider: Symbol.for("ArkServersNetServerDatabaseProvider"),

    DeutscheArkServerService: Symbol.for("DeutscheArkServerService"),
    ArkServersNetService: Symbol.for("ArkServersNetService"),
    VotingHistoryService: Symbol.for("VotingHistoryService"),
    DeutscheArkServerIdProvider: Symbol.for('DeutscheArkServerIdProvider'),
    DeutscheArkServerVotingService: Symbol.for('DeutscheArkServerVotingService'),
    ArkServersNetVotingService: Symbol.for('ArkServersNetVotingService')
};

export { TYPES_ARKVOTINGINTEGRATION };